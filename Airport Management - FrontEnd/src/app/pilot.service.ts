import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class PilotService {

  private baseUrl = 'http://localhost:8080/api/pilots';
  constructor(private http:HttpClient) { }

  addPilot(pilot : any): Observable<any>{
    return this.http.post(this.baseUrl,pilot);
  }
  getPilotsList():Observable<any>{
    return this.http.get(this.baseUrl);
  }
  getPilotById(id: number): Observable<any> {
    return this.http.get(`${this.baseUrl}/${id}`);
  }
  updatePilot(pilot: Object): Observable<Object> {
    return this.http.put(`${this.baseUrl}` + `/update`, pilot);
  }
}
