import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ManagerService {

  private baseUrl = 'http://localhost:8080/api/manager';
  private loginUrl = 'http://localhost:8080/api/managerlogins';
  private deleteUrl = 'http://localhost:8080/api/deletemanager';
 
  constructor(private http:HttpClient) { }

  createManager(manager : any): Observable<any>{
    return this.http.post(this.baseUrl,manager);
  }
  loginManager(manager) {
    return this.http.post<any>(this.loginUrl, manager);
  }
  getManagersList():Observable<any>{
    return this.http.get(this.baseUrl);
  }
  rejectManager(id : number) :Observable<any>{
    return this.http.delete(`${this.deleteUrl}/${id}`);
  }
}
