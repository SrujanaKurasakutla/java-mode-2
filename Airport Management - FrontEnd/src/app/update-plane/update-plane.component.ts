import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Plane } from '../Plane';
import { PlaneService } from '../plane.service';

@Component({
  selector: 'app-update-plane',
  templateUrl: './update-plane.component.html',
  styleUrls: ['./update-plane.component.css']
})
export class UpdatePlaneComponent implements OnInit {
  plane : Plane = new Plane();
  constructor(private planeService : PlaneService,private router : Router) { }

  ngOnInit() {
    this.updatePlane();
  }
  updatePlane(){
    let id=localStorage.getItem("planeId");
    this.planeService.getPlaneById(+id)
     .subscribe(data=>{
            this.plane=data;
        })
  }
  onUpdate(){
    console.log("into update");
    this.planeService.updatePlane(this.plane)
          .subscribe(data => {
     console.log(data);
     this.router.navigate(["welcomeAdmin"]);
    }, error => console.log(error));
        this.plane = new Plane();
    }
}
