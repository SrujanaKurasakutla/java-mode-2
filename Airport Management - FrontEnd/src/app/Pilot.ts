export class Pilot{
    pilotId : number;
    pilotName : string;
    age : number;
    gender : string;
}