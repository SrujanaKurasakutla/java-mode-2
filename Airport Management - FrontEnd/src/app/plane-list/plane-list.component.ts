import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { Plane } from '../Plane';
import { PlaneService } from '../plane.service';

@Component({
  selector: 'app-plane-list',
  templateUrl: './plane-list.component.html',
  styleUrls: ['./plane-list.component.css']
})
export class PlaneListComponent implements OnInit {

  planes : Observable<Plane[]>;
  p: Number = 1;
  count: Number = 2;
  constructor(private planeService : PlaneService,private router : Router) { }

  ngOnInit() {
    this.reloadData();
  }
  reloadData(){
    this.planes = this.planeService.getPlanesList();
  }
  getPlane(plane : Plane){
    localStorage.setItem("id",plane.planeId.toString());
    this.router.navigate(["getPlaneById"]);
  }
}
