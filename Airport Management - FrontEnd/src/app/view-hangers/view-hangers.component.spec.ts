import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewHangersComponent } from './view-hangers.component';

describe('ViewHangersComponent', () => {
  let component: ViewHangersComponent;
  let fixture: ComponentFixture<ViewHangersComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ViewHangersComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewHangersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
