import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { Hanger } from '../Hanger';
import { HangerService } from '../hanger.service';

@Component({
  selector: 'app-view-hangers',
  templateUrl: './view-hangers.component.html',
  styleUrls: ['./view-hangers.component.css']
})
export class ViewHangersComponent implements OnInit {
  hangers : Observable<Hanger[]>;
  p: Number = 1;
  count: Number = 2;
  constructor(private hangerService : HangerService,private router : Router) { }

  ngOnInit() {
    this.reloadData();
  }
  reloadData(){
    this.hangers = this.hangerService.getHangersList();
  }
  allocateHanger(hangers : Hanger){
    localStorage.setItem("hangerId",hangers.hangerId.toString());
    this.router.navigate(["updateHanger"]);

  }
}
