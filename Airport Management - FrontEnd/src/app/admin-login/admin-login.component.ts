import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AdminService } from '../admin.service';
import { AdminLogin } from '../AdminLogin';

@Component({
  selector: 'app-admin-login',
  templateUrl: './admin-login.component.html',
  styleUrls: ['./admin-login.component.css']
})
export class AdminLoginComponent implements OnInit {
  adminData : AdminLogin = new AdminLogin();
  constructor(private adminService : AdminService,private router : Router) { }

  ngOnInit(): void {
  }
  loginAdmin () {
    this.adminService.loginAdmin(this.adminData)
    .subscribe(
      data => {
        console.log(data);
        this.router.navigate(['welcomeAdmin'])
      },
      err => console.log(err)
    ) 
  }
}
