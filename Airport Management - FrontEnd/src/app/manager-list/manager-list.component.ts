import { Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { Manager } from '../Manager';
import { ManagerService } from '../manager.service';

@Component({
  selector: 'app-manager-list',
  templateUrl: './manager-list.component.html',
  styleUrls: ['./manager-list.component.css']
})
export class ManagerListComponent implements OnInit {
  managers : Observable<Manager[]>;
  p: Number = 1;
  count: Number = 2;
  constructor(private managerService : ManagerService,private router : Router) { }

  ngOnInit() {
    this.reloadData();
  }
  reloadData(){
    this.managers = this.managerService.getManagersList();
  }
  acceptManager(){
    console.log("Request Accepted");
    this.router.navigate(['welcomeAdmin']);
  }
  rejectManager(manager : Manager) {
    this.managerService.rejectManager(manager.managerId)
      .subscribe(
        data => {
          console.log(data);
          this.reloadData();
        },
        error => console.log(error));
  }
}
