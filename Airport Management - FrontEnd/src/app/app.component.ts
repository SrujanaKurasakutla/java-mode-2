import { Component } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'Airport-Management';
  constructor(private router : Router){}
  logout(){
    console.log("logged Off");
    this.router.navigate(['adminLogin']);
  }
}
