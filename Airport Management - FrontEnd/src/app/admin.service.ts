import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AdminService {
  private baseUrl = 'http://localhost:8080/api/admin';
  private loginUrl = 'http://localhost:8080/api/login';
  constructor(private http:HttpClient) { }

  createAdmin(admin : any): Observable<any>{
    return this.http.post(this.baseUrl,admin);
  }
  loginAdmin(admin) {
    return this.http.post<any>(this.loginUrl, admin)
  }
}
