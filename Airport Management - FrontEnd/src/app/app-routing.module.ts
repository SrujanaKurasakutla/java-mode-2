import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AddHangerComponent } from './add-hanger/add-hanger.component';
import { AddPilotComponent } from './add-pilot/add-pilot.component';
import { AddPlaneComponent } from './add-plane/add-plane.component';
import { AdminLoginComponent } from './admin-login/admin-login.component';
import { AdminRegisterComponent } from './admin-register/admin-register.component';
import { AllocateHangersComponent } from './allocate-hangers/allocate-hangers.component';
import { AppComponent } from './app.component';
import { HangerStatusComponent } from './hanger-status/hanger-status.component';
import { HomeComponent } from './home/home.component';
import { ManagerListComponent } from './manager-list/manager-list.component';
import { ManagerLoginComponent } from './manager-login/manager-login.component';
import { ManagerRegisterComponent } from './manager-register/manager-register.component';
import { PageNotfoundComponent } from './page-notfound/page-notfound.component';
import { PilotDetailsComponent } from './pilot-details/pilot-details.component';
import { PlaneDetailsComponent } from './plane-details/plane-details.component';
import { PlaneListComponent } from './plane-list/plane-list.component';
import { UpdatePilotComponent } from './update-pilot/update-pilot.component';
import { UpdatePlaneComponent } from './update-plane/update-plane.component';
import { ViewHangersComponent } from './view-hangers/view-hangers.component';
import { ViewPilotsComponent } from './view-pilots/view-pilots.component';
import { WelcomeAdminComponent } from './welcome-admin/welcome-admin.component';
import { WelcomeManagerComponent } from './welcome-manager/welcome-manager.component';
 
const routes: Routes = [
  {path:'adminRegister',component:AdminRegisterComponent},
  {path:'getPlaneById',component:PlaneDetailsComponent},
  {path:'updatePlane',component:UpdatePlaneComponent},
  {path:'adminLogin',component:AdminLoginComponent},
  {path:'welcomeAdmin',component:WelcomeAdminComponent,children:[
    {path:'addPlanes',component:AddPlaneComponent},
    {path:'getAllPlanes',component:PlaneListComponent},
    {path:'getAllManagers',component:ManagerListComponent},
    {path:'addHanger',component:AddHangerComponent},
    {path:'getAllPilots',component:ViewPilotsComponent},
    {path:'addPilots',component:AddPilotComponent},
  ]},
  {path:'managerLogin',component:ManagerLoginComponent},
  {path:'managerRegister',component:ManagerRegisterComponent},
  {path:'welcomeManager',component:WelcomeManagerComponent,children:[
    {path:'allocateHangers',component:ViewHangersComponent},
    {path:'hangerStatus',component:HangerStatusComponent}
   
  ]},
  {path:'',component:HomeComponent},
  {path:'updateHanger',component:AllocateHangersComponent},
  {path:'updatePilot',component:UpdatePilotComponent},
  {path:'getPilotById',component:PilotDetailsComponent},
  {path:'not-found',component:PageNotfoundComponent},
  {path:'**',redirectTo:'/not-found'}
  
 
];
 
@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
