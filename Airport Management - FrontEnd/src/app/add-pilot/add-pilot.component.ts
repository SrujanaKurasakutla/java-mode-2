import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Pilot } from '../Pilot';
import { PilotService } from '../pilot.service';

@Component({
  selector: 'app-add-pilot',
  templateUrl: './add-pilot.component.html',
  styleUrls: ['./add-pilot.component.css']
})
export class AddPilotComponent implements OnInit {

 pilot : Pilot = new Pilot();
  submitted = false;
  constructor(private pilotService : PilotService,private router : Router) { }

  ngOnInit() {
  }
  
  save(){
    this.pilotService.addPilot(this.pilot)
     .subscribe(
       data => {
         console.log(data);
         this.submitted = true;
       },
       error => console.log(error)
     );
     this.pilot = new Pilot();
  }
  onSubmit(){
  this.save();
  }

}
