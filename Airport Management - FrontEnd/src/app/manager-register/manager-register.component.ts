import { Component, OnInit } from '@angular/core';
import { Manager } from '../Manager';
import { ManagerService } from '../manager.service';

@Component({
  selector: 'app-manager-register',
  templateUrl: './manager-register.component.html',
  styleUrls: ['./manager-register.component.css']
})
export class ManagerRegisterComponent implements OnInit {

  manager :Manager = new Manager();
  submitted = false;
  constructor(private managerService : ManagerService) { }

  ngOnInit(): void {
  }
  save(){
    this.managerService.createManager(this.manager)
     .subscribe(
       data => {
         console.log(data);
         this.submitted = true;
       },
       error => console.log(error)
     );
     this.manager = new Manager();
  }
  onSubmit(){
  this.save();
  }

}
