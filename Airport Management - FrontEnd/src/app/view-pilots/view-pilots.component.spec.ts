import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewPilotsComponent } from './view-pilots.component';

describe('ViewPilotsComponent', () => {
  let component: ViewPilotsComponent;
  let fixture: ComponentFixture<ViewPilotsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ViewPilotsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewPilotsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
