import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { Pilot } from '../Pilot';
import { PilotService } from '../pilot.service';

@Component({
  selector: 'app-view-pilots',
  templateUrl: './view-pilots.component.html',
  styleUrls: ['./view-pilots.component.css']
})
export class ViewPilotsComponent implements OnInit {

  pilots : Observable<Pilot[]>;
  p: Number = 1;
  count: Number = 2;
  constructor(private pilotService : PilotService,private router : Router) { }

  ngOnInit() {
    this.reloadData();
  }
  reloadData(){
    this.pilots = this.pilotService.getPilotsList();
  }
  getPilot(pilot : Pilot){
    localStorage.setItem("pilotId",pilot.pilotId.toString());
    this.router.navigate(["getPilotById"]);
  }

}
