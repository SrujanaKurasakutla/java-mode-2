import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { Plane } from '../Plane';
import { PlaneService } from '../plane.service';

@Component({
  selector: 'app-plane-details',
  templateUrl: './plane-details.component.html',
  styleUrls: ['./plane-details.component.css']
})
export class PlaneDetailsComponent implements OnInit {
  plane : Plane = new Plane();
  constructor(private planeService : PlaneService,private router : Router) { }

  ngOnInit() {
   this.getPlane();
  }
  getPlane(){
    let id=localStorage.getItem("id");
    this.planeService.getPlaneById(+id)
     .subscribe(data=>{
            this.plane=data;
        })
  }
  updatePlane(planes : Plane){
    localStorage.setItem("planeId",planes.planeId.toString());
    this.router.navigate(["updatePlane"]);

  }
}
