import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ManagerService } from '../manager.service';
import { ManagerLogin } from '../ManagerLogin';

@Component({
  selector: 'app-manager-login',
  templateUrl: './manager-login.component.html',
  styleUrls: ['./manager-login.component.css']
})
export class ManagerLoginComponent implements OnInit {

  managerData : ManagerLogin = new ManagerLogin();
  constructor(private managerService : ManagerService,private router : Router) { }

  ngOnInit(): void {
  }
  loginManager () {
    this.managerService.loginManager(this.managerData)
    .subscribe(
      data => {
        console.log(data);
        this.router.navigate(['welcomeManager'])
      },
      err => console.log(err)
    ) 
  }

}
