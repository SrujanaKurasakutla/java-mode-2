export class Admin{
    firstName : string;
    lastName : string;
    age : number;
    gender : string;
    contactNumber : number;
    vendorId : number;
    password : string;
}