export class Manager{
    firstName : string;
    lastName : string;
    age : number;
    gender : string;
    contactNumber : number;
    managerId : number;
    password : string;
}