import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Pilot } from '../Pilot';
import { PilotService } from '../pilot.service';

@Component({
  selector: 'app-update-pilot',
  templateUrl: './update-pilot.component.html',
  styleUrls: ['./update-pilot.component.css']
})
export class UpdatePilotComponent implements OnInit {

  pilot : Pilot = new Pilot();
  constructor(private pilotService : PilotService,private router : Router) { }

  ngOnInit() {
    this.updatePilot();
  }
  updatePilot(){
    let id=localStorage.getItem("pilotId");
    this.pilotService.getPilotById(+id)
     .subscribe(data=>{
            this.pilot=data;
        })
  }
  onUpdate(){
    console.log("into update");
    this.pilotService.updatePilot(this.pilot)
          .subscribe(data => {
     console.log(data);
     this.router.navigate(["welcomeAdmin"]);
    }, error => console.log(error));
        this.pilot = new Pilot();
    }

}
