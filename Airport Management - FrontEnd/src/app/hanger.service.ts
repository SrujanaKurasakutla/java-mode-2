import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class HangerService {

  private baseUrl = 'http://localhost:8080/api/hangers';
  constructor(private http:HttpClient) { }

  addHanger(hanger : any): Observable<any>{
    return this.http.post(this.baseUrl,hanger);
  }
  getHangersList():Observable<any>{
    return this.http.get(this.baseUrl);
  }
  updateHanger(hanger: Object): Observable<Object> {
    return this.http.put(`${this.baseUrl}` + `/update`, hanger);
  }
  getHangerById(id: number): Observable<any> {
    return this.http.get(`${this.baseUrl}/${id}`);
  }
}
