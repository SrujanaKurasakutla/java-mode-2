import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Plane } from '../Plane';
import { PlaneService } from '../plane.service';

@Component({
  selector: 'app-add-plane',
  templateUrl: './add-plane.component.html',
  styleUrls: ['./add-plane.component.css']
})
export class AddPlaneComponent implements OnInit {
  plane : Plane = new Plane();
  submitted = false;
  constructor(private planeService : PlaneService,private router : Router) { }

  ngOnInit() {
  }
  
  save(){
    this.planeService.addPlane(this.plane)
     .subscribe(
       data => {
         console.log(data);
         this.submitted = true;
       },
       error => console.log(error)
     );
     this.plane = new Plane();
  }
  onSubmit(){
  this.save();
  }
}
