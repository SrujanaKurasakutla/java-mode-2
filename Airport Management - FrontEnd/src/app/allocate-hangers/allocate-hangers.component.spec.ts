import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AllocateHangersComponent } from './allocate-hangers.component';

describe('AllocateHangersComponent', () => {
  let component: AllocateHangersComponent;
  let fixture: ComponentFixture<AllocateHangersComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AllocateHangersComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AllocateHangersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
