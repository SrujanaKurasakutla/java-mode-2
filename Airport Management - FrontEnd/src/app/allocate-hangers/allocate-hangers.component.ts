import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Hanger } from '../Hanger';
import { HangerService } from '../hanger.service';

@Component({
  selector: 'app-allocate-hangers',
  templateUrl: './allocate-hangers.component.html',
  styleUrls: ['./allocate-hangers.component.css']
})
export class AllocateHangersComponent implements OnInit {
  hanger : Hanger = new Hanger();
  constructor(private hangerService : HangerService,private router : Router) { }

  ngOnInit() {
    this.allocateHanger();
  }
  allocateHanger(){
    let id=localStorage.getItem("hangerId");
    this.hangerService.getHangerById(+id)
     .subscribe(data=>{
            this.hanger=data;
        })
  }
  onUpdate(){
    console.log("into update");
    this.hangerService.updateHanger(this.hanger)
          .subscribe(data => {
     console.log(data);
     this.router.navigate(["welcomeManager"]);
    }, error => console.log(error));
        this.hanger = new Hanger();
    }
}
