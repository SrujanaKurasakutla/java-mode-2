import { Component, OnInit } from '@angular/core';
import { Hanger } from '../Hanger';
import { HangerService } from '../hanger.service';

@Component({
  selector: 'app-add-hanger',
  templateUrl: './add-hanger.component.html',
  styleUrls: ['./add-hanger.component.css']
})
export class AddHangerComponent implements OnInit {
  submitted = false;
  hanger : Hanger = new Hanger();
  constructor(private hangerService : HangerService) { }

  ngOnInit(): void {
  }
  save(){
    this.hangerService.addHanger(this.hanger)
     .subscribe(
       data => {
         console.log(data);
         console.log(this.hanger.plane);
         this.submitted = true;
       },
       error => console.log(error)
     );
     this.hanger = new Hanger();
  }
  onSubmit(){
  this.save();
  }
}
