import { ComponentFixture, TestBed } from '@angular/core/testing';

import { HangerStatusComponent } from './hanger-status.component';

describe('HangerStatusComponent', () => {
  let component: HangerStatusComponent;
  let fixture: ComponentFixture<HangerStatusComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ HangerStatusComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(HangerStatusComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
