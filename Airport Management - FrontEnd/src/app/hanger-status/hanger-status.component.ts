import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { Hanger } from '../Hanger';
import { HangerService } from '../hanger.service';

@Component({
  selector: 'app-hanger-status',
  templateUrl: './hanger-status.component.html',
  styleUrls: ['./hanger-status.component.css']
})
export class HangerStatusComponent implements OnInit {
  
  hangers : Observable<Hanger[]>;
  p: Number = 1;
  count: Number = 2;
  constructor(private hangerService : HangerService,private router : Router) { }

  ngOnInit() {
    this.reloadData();
  }
  reloadData(){
    this.hangers = this.hangerService.getHangersList();
  }

}
