import { TestBed } from '@angular/core/testing';

import { HangerService } from './hanger.service';

describe('HangerService', () => {
  let service: HangerService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(HangerService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
