import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Pilot } from '../Pilot';
import { PilotService } from '../pilot.service';

@Component({
  selector: 'app-pilot-details',
  templateUrl: './pilot-details.component.html',
  styleUrls: ['./pilot-details.component.css']
})
export class PilotDetailsComponent implements OnInit {

  pilot : Pilot = new Pilot();
  constructor(private pilotService : PilotService,private router : Router) { }

  ngOnInit() {
   this.getPilot();
  }
  getPilot(){
    let id=localStorage.getItem("pilotId");
    this.pilotService.getPilotById(+id)
     .subscribe(data=>{
            this.pilot=data;
        })
  }
  updatePlane(pilots : Pilot){
    localStorage.setItem("pilotId",pilots.pilotId.toString());
    this.router.navigate(["updatePilot"]);

  }

}
