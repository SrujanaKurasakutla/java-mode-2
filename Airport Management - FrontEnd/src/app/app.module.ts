import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { AdminRegisterComponent } from './admin-register/admin-register.component';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { AddPlaneComponent } from './add-plane/add-plane.component';
import { PlaneDetailsComponent } from './plane-details/plane-details.component';
import { PlaneListComponent } from './plane-list/plane-list.component';
import { UpdatePlaneComponent } from './update-plane/update-plane.component';
import { AddPilotComponent } from './add-pilot/add-pilot.component';
import { AdminLoginComponent } from './admin-login/admin-login.component';
import { WelcomeAdminComponent } from './welcome-admin/welcome-admin.component';
import { ManagerLoginComponent } from './manager-login/manager-login.component';
import { ManagerRegisterComponent } from './manager-register/manager-register.component';
import { WelcomeManagerComponent } from './welcome-manager/welcome-manager.component';
import { ManagerListComponent } from './manager-list/manager-list.component';
import { AddHangerComponent } from './add-hanger/add-hanger.component';
import { ViewHangersComponent } from './view-hangers/view-hangers.component';
import { AllocateHangersComponent } from './allocate-hangers/allocate-hangers.component';
import { HangerStatusComponent } from './hanger-status/hanger-status.component';
import { ViewPilotsComponent } from './view-pilots/view-pilots.component';
import { PilotDetailsComponent } from './pilot-details/pilot-details.component';
import { UpdatePilotComponent } from './update-pilot/update-pilot.component';
import { PageNotfoundComponent } from './page-notfound/page-notfound.component';
import { HomeComponent } from './home/home.component';
import { NgxPaginationModule } from 'ngx-pagination';
@NgModule({
  declarations: [
    AppComponent,
    AdminRegisterComponent,
    AddPlaneComponent,
    PlaneDetailsComponent,
    PlaneListComponent,
    UpdatePlaneComponent,
    AddPilotComponent,
    AdminLoginComponent,
    WelcomeAdminComponent,
    ManagerLoginComponent,
    ManagerRegisterComponent,
    WelcomeManagerComponent,
    ManagerListComponent,
    AddHangerComponent,
    ViewHangersComponent,
    AllocateHangersComponent,
    HangerStatusComponent,
    ViewPilotsComponent,
    PilotDetailsComponent,
    UpdatePilotComponent,
    PageNotfoundComponent,
    HomeComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule,
    NgxPaginationModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
