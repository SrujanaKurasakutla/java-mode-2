export class Plane{
    planeId : number;
    carrierName : string;
    planeModel : string;
    seatCapacity : number;
}