import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class PlaneService {

  private baseUrl = 'http://localhost:8080/api/planes';
  constructor(private http:HttpClient) { }

  addPlane(plane : any): Observable<any>{
    return this.http.post(this.baseUrl,plane);
  }
  getPlanesList():Observable<any>{
    return this.http.get(this.baseUrl);
  }
  getPlaneById(id: number): Observable<any> {
    return this.http.get(`${this.baseUrl}/${id}`);
  }
  updatePlane(plane: Object): Observable<Object> {
    return this.http.put(`${this.baseUrl}` + `/update`, plane);
  }
}
