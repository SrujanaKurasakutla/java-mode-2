import { Skill } from "./Skill";
import { Department } from "./Department";

export interface Employee {
    id:number;
    name:string;
    salary:number;
    permanent:boolean;
    department:Department;
    skills:Skill[];
    dateOfBirth:Date;
}