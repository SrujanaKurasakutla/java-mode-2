import { Component, OnInit } from '@angular/core';
import { Employee } from '../Employee';

@Component({
  selector: 'app-employee',
  templateUrl: './employee.component.html',
  styleUrls: ['./employee.component.css'],
})
export class EmployeeComponent implements OnInit {
  emp:Employee = {
    id:3,
    name:"John",
    salary:10000,
    permanent:true,
    department:{dept_Id:1,dept_Name:"PayRoll"},
    skills:[
      {skill_Id:1,skill_Name:"HTML"},
      {skill_Id:2,skill_Name:"CSS"},
      {skill_Id:3,skill_Name:"JavaScript"}
    ],
    dateOfBirth:new Date('02/18/1999')
  };
  constructor() {}

  ngOnInit(): void {}

}
