package com.pack.AirportManagement.model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "hanger")
public class Hanger {
	@Id
	private long hangerId;
	private String plane;
	private String status;
	public Hanger() {
		super();
		// TODO Auto-generated constructor stub
	}
	public Hanger(long hangerId, String plane, String status) {
		super();
		this.hangerId = hangerId;
		this.plane = plane;
		this.status = status;
	}
	public Hanger(long hangerId, String plane) {
		super();
		this.hangerId = hangerId;
		this.plane = plane;
	}
	public long getHangerId() {
		return hangerId;
	}
	public void setHangerId(long hangerId) {
		this.hangerId = hangerId;
	}
	public String getPlane() {
		return plane;
	}
	public void setPlane(String plane) {
		this.plane = plane;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	@Override
	public String toString() {
		return "Hanger [hangerId=" + hangerId + ", plane=" + plane + ", status=" + status + "]";
	}
	
	
}
