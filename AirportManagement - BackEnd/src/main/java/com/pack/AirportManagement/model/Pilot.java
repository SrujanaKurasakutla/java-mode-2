package com.pack.AirportManagement.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "pilot")
public class Pilot {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long pilotId;
	private String pilotName;
	private int age;
	private String gender;
	public Pilot() {
		super();
		// TODO Auto-generated constructor stub
	}
	public Pilot(String pilotName, int age, String gender) {
		super();
		this.pilotName = pilotName;
		this.age = age;
		this.gender = gender;
	}
	public Pilot(long pilotId, String pilotName, int age, String gender) {
		super();
		this.pilotId = pilotId;
		this.pilotName = pilotName;
		this.age = age;
		this.gender = gender;
	}
	public long getPilotId() {
		return pilotId;
	}
	public void setPilotId(long pilotId) {
		this.pilotId = pilotId;
	}
	public String getPilotName() {
		return pilotName;
	}
	public void setPilotName(String pilotName) {
		this.pilotName = pilotName;
	}
	public int getAge() {
		return age;
	}
	public void setAge(int age) {
		this.age = age;
	}
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	@Override
	public String toString() {
		return "Pilot [pilotId=" + pilotId + ", pilotName=" + pilotName + ", age=" + age + ", gender=" + gender + "]";
	}
}
