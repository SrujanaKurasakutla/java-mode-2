package com.pack.AirportManagement.dao;

import org.springframework.data.repository.CrudRepository;

import com.pack.AirportManagement.model.Pilot;

public interface PilotDao extends CrudRepository<Pilot, Long>{

}
