package com.pack.AirportManagement.dao;

import org.springframework.data.repository.CrudRepository;

import com.pack.AirportManagement.model.Hanger;

public interface HangerDao extends CrudRepository<Hanger, Long>{

}
