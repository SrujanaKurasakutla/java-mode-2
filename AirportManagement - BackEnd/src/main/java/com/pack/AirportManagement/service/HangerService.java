package com.pack.AirportManagement.service;

import org.springframework.http.ResponseEntity;

import com.pack.AirportManagement.model.Hanger;

public interface HangerService {

	public ResponseEntity<?> addHanger(Hanger hanger);

	public Iterable<Hanger> getAllHangers();

	public ResponseEntity<Hanger> getHangerById(long hangerId);

	public Hanger allocateHanger(Hanger hanger);

}
