package com.pack.AirportManagement.service;

import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import com.pack.AirportManagement.dao.PilotDao;
import com.pack.AirportManagement.model.Pilot;

@Service
public class PilotServiceImpl implements PilotService{
	private static final Logger LOGGER = LoggerFactory.getLogger(PilotServiceImpl.class);
    @Autowired
    PilotDao pilotDao;
	@Override
	public ResponseEntity<?> addPilot(Pilot pilot) {
		try {
			Pilot _pilot = pilotDao
					.save(new Pilot(pilot.getPilotName(),pilot.getAge(),pilot.getGender()));
			  LOGGER.info("Pilot Details Added");
			return new ResponseEntity<>(_pilot, HttpStatus.CREATED);
		} catch (Exception e) {
			 LOGGER.info("Exception in add pilot");
			return new ResponseEntity<>(null, HttpStatus.EXPECTATION_FAILED);
		}
	}

	@Override
	public Iterable<Pilot> getAllPilots() {
		return pilotDao.findAll();
	}

	@Override
	public ResponseEntity<Pilot> getPilotById(long pilotId) {
		Optional<Pilot> pilotData = pilotDao.findById(pilotId);

	    if (pilotData.isPresent()) {
	    	LOGGER.info("Return pilot details");
	      return new ResponseEntity<>(pilotData.get(), HttpStatus.OK);
	    } else {
	    	LOGGER.info("piot not found");
	      return new ResponseEntity<>(HttpStatus.NOT_FOUND);
	    }
	}

	@Override
	public Pilot updatePilot(Pilot pilot) {
		 System.out.println("Information update");
		  //  System.out.println("into update"+customer.getId()+" "+customer.getName());
		        Pilot pilot1 = pilotDao.save(new Pilot(pilot.getPilotId(),pilot.getPilotName(),pilot.getAge(),pilot.getGender()));
		        LOGGER.info("Updated pilot details");
		    return pilot1;
	}

}
