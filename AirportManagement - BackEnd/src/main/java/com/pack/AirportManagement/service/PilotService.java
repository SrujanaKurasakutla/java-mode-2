package com.pack.AirportManagement.service;

import org.springframework.http.ResponseEntity;

import com.pack.AirportManagement.model.Pilot;


public interface PilotService {
	public ResponseEntity<?> addPilot(Pilot pilot);

	public Iterable<Pilot> getAllPilots();
	
	public ResponseEntity<Pilot> getPilotById(long pilotId);

	public Pilot updatePilot(Pilot pilot);
}
