package com.pack.AirportManagement.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import java.util.Optional;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import com.pack.AirportManagement.dao.PlaneDao;
import com.pack.AirportManagement.model.Plane;

@Service
public class PlaneServiceImpl implements PlaneService {
	private static final Logger LOGGER = LoggerFactory.getLogger(PlaneServiceImpl.class);
	@Autowired
	PlaneDao planeDao;

	@Override
	public ResponseEntity<Plane> addPlane(Plane plane) {
		try {
			Plane _plane = planeDao
					.save(new Plane(plane.getCarrierName(), plane.getPlaneModel(), plane.getSeatCapacity()));
			LOGGER.info("Plane details added");
			return new ResponseEntity<>(_plane, HttpStatus.CREATED);
		} catch (Exception e) {
			LOGGER.info("Exception in add plane");
			return new ResponseEntity<>(null, HttpStatus.EXPECTATION_FAILED);
		}

	}

	@Override
	public Iterable<Plane> getAllPlanes() {
		return planeDao.findAll();
	}

	@Override
	public ResponseEntity<Plane> getPlaneById(long planeId) {
		Optional<Plane> planeData = planeDao.findById(planeId);

		if (planeData.isPresent()) {
			LOGGER.info("Returns plane Details");
			return new ResponseEntity<>(planeData.get(), HttpStatus.OK);
		} else {
			LOGGER.info("Plane not found");
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}

	@Override
	public Plane updatePlane(Plane plane) {
		System.out.println("Information update");
		// System.out.println("into update"+customer.getId()+" "+customer.getName());
		Plane plane1 = planeDao.save(
				new Plane(plane.getPlaneId(), plane.getCarrierName(), plane.getPlaneModel(), plane.getSeatCapacity()));
		LOGGER.info("Updated Plane Details");
		return plane1;
	}

}
