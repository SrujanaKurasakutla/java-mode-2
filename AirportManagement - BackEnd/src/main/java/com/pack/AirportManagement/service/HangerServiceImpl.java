package com.pack.AirportManagement.service;

import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.pack.AirportManagement.dao.HangerDao;
import com.pack.AirportManagement.model.Hanger;

@Service
public class HangerServiceImpl implements HangerService {
	private static final Logger LOGGER = LoggerFactory.getLogger(HangerServiceImpl.class);
    @Autowired
    HangerDao hangerDao;
	@Override
	public ResponseEntity<?> addHanger(Hanger hanger) {
		try {
		if(hanger.getPlane()!=null) {
			Hanger _hanger = hangerDao.save(new Hanger(hanger.getHangerId(),hanger.getPlane(),"Allocated"));
			LOGGER.info("Hanger details added");
			return new ResponseEntity<>(_hanger, HttpStatus.CREATED);
		}
		else {
			Hanger _hanger = hangerDao.save(new Hanger(hanger.getHangerId(),hanger.getPlane(),"Available"));
			LOGGER.info("Hanger details added");
			return new ResponseEntity<>(_hanger, HttpStatus.CREATED);
		}
		}catch(Exception e) {
			LOGGER.info("Exception in add hanger");
			return new ResponseEntity<>(null, HttpStatus.EXPECTATION_FAILED);
		}
	}
	@Override
	public Iterable<Hanger> getAllHangers() {
		return hangerDao.findAll();
	}
	@Override
	public ResponseEntity<Hanger> getHangerById(long hangerId) {
		Optional<Hanger> hangerData = hangerDao.findById(hangerId);

	    if (hangerData.isPresent()) {
	    	LOGGER.info("Returns hanger details");
	      return new ResponseEntity<>(hangerData.get(), HttpStatus.OK);
	    } else {
	    	LOGGER.info("Hanger Not found");
	      return new ResponseEntity<>(HttpStatus.NOT_FOUND);
	    }
	}
	@Override
	public Hanger allocateHanger(Hanger hanger) {
			Hanger _hanger = hangerDao.save(new Hanger(hanger.getHangerId(),hanger.getPlane(),"Allocated"));
			LOGGER.info("Hanger details updated");
			return _hanger;
			
	}

}
