package com.pack.AirportManagement.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.pack.AirportManagement.model.Pilot;
import com.pack.AirportManagement.service.PilotService;

@CrossOrigin(origins = "http://localhost:3000")
@RestController
@RequestMapping("/api")
public class PilotController {

	@Autowired
	PilotService pilotService;

	@PostMapping("/pilots")
	public void addPilot(@RequestBody Pilot pilot) {
		pilotService.addPilot(pilot);
	}

	@GetMapping("/pilots")
	public Iterable<Pilot> getAllPilots() {
		return pilotService.getAllPilots();
	}

	@GetMapping("/pilots/{id}")
	public ResponseEntity<Pilot> getPilotById(@PathVariable("id") long pilotId) {
		return pilotService.getPilotById(pilotId);
	}

	@PutMapping(value = "/pilots/update")
	public Pilot updatePilot(@RequestBody Pilot pilot) {
		return pilotService.updatePilot(pilot);
	}
}
