package com.pack.AirportManagement.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.pack.AirportManagement.model.Hanger;
import com.pack.AirportManagement.service.HangerService;

@CrossOrigin(origins = "http://localhost:3000")
@RestController
@RequestMapping("/api")
public class HangerController {
	@Autowired
	HangerService hangerService;
	
	@PostMapping("/hangers")
	public void addHanger(@RequestBody Hanger hanger) {
		hangerService.addHanger(hanger);
	}
	@GetMapping("/hangers")
	public Iterable<Hanger> getAllHangers() {
		return hangerService.getAllHangers();
	}
	@GetMapping("/hangers/{id}")
	public ResponseEntity<Hanger> getHangerById(@PathVariable("id") long hangerId) {
		return hangerService.getHangerById(hangerId);
	}
	@PutMapping(value = "/hangers/update")
	  public Hanger allocateHanger(@RequestBody Hanger hanger) {
		return hangerService.allocateHanger(hanger);
	}

}