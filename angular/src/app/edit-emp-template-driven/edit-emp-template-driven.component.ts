import { Component, OnInit } from '@angular/core';
import { Department } from '../Department';
import { Employee } from '../Employee';

@Component({
  selector: 'app-edit-emp-template-driven',
  templateUrl: './edit-emp-template-driven.component.html',
  styleUrls: ['./edit-emp-template-driven.component.css']
})
export class EditEmpTemplateDrivenComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

  emp: Employee = {
    id: 10,
    name: 'Srujana',
    salary: 30000,
    permanent:true,
    department:{dept_Id:1,dept_Name:'Payroll'},
    skills: [
      { skill_Id: 1, skill_Name: 'HTML' },
      { skill_Id: 2, skill_Name: 'CSS' },
      { skill_Id: 3, skill_Name: 'JAVASCRIPT' },
    ],
    dateOfBirth: new Date('08/17/2020')
  };

  dept : Department[]=[
    { dept_Id: 1, dept_Name: "Payroll" },
    { dept_Id: 2, dept_Name: "Internal" },
    { dept_Id: 3, dept_Name: "HR" }
  ];

  onSubmit() {​​​​​​​
    console.log(this.emp);
    }​​​​​​​

}
