import { Department } from './Department';
import { skill } from './skill';

export interface Employee{
    id:number;
    name:string;
    salary:number;
    permanent:boolean;
    department:Department;
    skills:skill[];
    dateOfBirth:Date;
}