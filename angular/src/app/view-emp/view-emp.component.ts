import { Component, OnInit } from '@angular/core';
import { Employee } from '../Employee';

@Component({
  selector: 'app-view-emp',
  templateUrl: './view-emp.component.html',
  styleUrls: ['./view-emp.component.css']
})
export class ViewEmpComponent implements OnInit {
  emp:Employee = {
    id:3,
    name:"John",
    salary:10000,
    permanent:true,
    department:{dept_Id:1,dept_Name:"PayRoll"},
    skills:[
      {skill_Id:1,skill_Name:"HTML"},
      {skill_Id:2,skill_Name:"CSS"},
      {skill_Id:3,skill_Name:"JavaScript"}
    ],
    dateOfBirth:new Date('02/18/1999')
    
  };


  constructor() { }

  ngOnInit(): void {
  }

}
